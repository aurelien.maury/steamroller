env-clean:
	@rm -rf ${PWD}/.direnv && direnv reload
	@echo "------------------------------------------"
	@echo "+ .direnv directory delete"
	@echo "+ .direnv rebuilt"
	@echo ""
	@echo "To complete, run:"
	@echo "   make env"
	@echo "------------------------------------------"

env:
	direnv allow
	pip3 install -U --no-cache pip setuptools wheel
	pip3 install -U --no-cache -r requirements.txt 
	ansible-galaxy collection install -fr requirements.yml

ping:
	ansible -m ping all

doc-clean:
	@cd docs && make clean
doc:
	@cd docs && make html
	@echo "------------------------------------------"
	@echo "Static documentation exported:"
	@echo "file://${PWD}/docs/build/html/index.html"
	@echo "------------------------------------------"