# Ansible Collection - lafrancaise.hardening

## Prerequisites

* `make`
* `python >= 3.7.3`
* `venv`
* `pip3`
* [`direnv`](https://direnv.net/docs/installation.html)
* `direnv` [hooked to your shell](https://direnv.net/docs/hook.html)

## Setup workspace

```
git clone https://gitlab.com/wescalefr/lafrancaise.git
cd lafrancaise
direnv allow .
make env
```
