.. _ref_playbook_hardening:

##########################################
playbooks/hardening.yml
##########################################

Goal
====================

Apply the ``hardening`` role on the ``lafrancaise`` host group.

Launch
====================

.. code:: bash

    ansible-playbook playbooks/hardening.yml
    