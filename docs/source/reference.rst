##########################################
Reference Guides
##########################################

Playbooks
====================

.. toctree::
   :maxdepth: 1

   reference/playbook_hardening

Rôles
====================

.. toctree::
   :maxdepth: 1

   reference/role_hardening
