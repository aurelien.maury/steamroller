.. _how_to_add_host:

##########################################
Add host to inventory
##########################################

Set
====================

Let's assume for this guide that we have a host to manage and:

* ``${HOST_NICKNAME}`` should be replaced by the nickname of the host in your project. It can be different from the actual ``hostname`` of the server.
* ``${HOST_IP}`` should be replaced by the actual ip address of the host
* ``${HOST_USER}`` should be replaced by the connection user to the host
* ``${HOST_PRIVATE_KEY}`` should be replaced by the path to the private key file to use for ssh connection.

Pay attention to code and configuration example and replace these occurences by your own values.

SSH configuration
====================

Add a named section to your project ``ssh.cfg`` file:

.. code-block:: bash
    :caption: ``ssh.cfg``

    Host ${HOST_NICKNAME}
      Hostname ${HOST_IP}
      IdentityFile ${HOST_PRIVATE_KEY}
      User ${HOST_USER}
      IdentitiesOnly yes
      StrictHostKeyChecking no

Inventory update
====================

Add the host to a group in your project ``host_inventory`` file:

.. code-block:: ini
    :caption: ``host_inventory``

    [lafrancaise]
    ${HOST_NICKNAME}

Integration validation
======================

Validate ansible integration by running:

.. code-block:: bash
   :emphasize-lines: 1

    ansible -m ping ${HOST_NICKNAME}

    ${HOST_NICKNAME} | SUCCESS => {
        "ansible_facts": {
            "discovered_interpreter_python": "/usr/bin/python"
        },
        "changed": false,
        "ping": "pong"
    }
