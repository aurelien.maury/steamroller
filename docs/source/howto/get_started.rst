.. _how_to_get_started:

##########################################
Get started
##########################################

Prerequisites
====================

* ``make``
* ``python >= 3.7.3``
* ``virtualenv``
* ``pip3``
* `direnv <https://direnv.net/docs/installation.html>`_ 
* direnv `hooked to your shell <https://direnv.net/docs/hook.html>`_

Setup workspace
====================

* Run:

.. code:: bash

    git clone https://gitlab.com/wescalefr/lafrancaise.git
    cd lafrancaise
    direnv allow .
    make env

.. admonition:: CONGRATULATIONS
    :class: important

    You are ready to work with the project.
