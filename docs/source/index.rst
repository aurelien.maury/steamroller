##########################################
LaFrancaise - Ansible toolkit
##########################################

New on the project? :ref:`Start here. <how_to_get_started>`

Sections
==================

.. toctree::
   :maxdepth: 1

   howto
   reference

Credits
==================

* Documentation structure by Daniele Procida. `Learn more... <https://documentation.divio.com/>`_